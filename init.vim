"==================
" Plugin Management
"==================
let s:vim_plug_file="~/.local/share/nvim/site/autoload/plug.vim"
let s:vim_plug_dir="~/.local/share/nvim/plugged"
let s:local_init_file="~/.config/nvim/local_init.vim"
let s:local_plugins_file="~/.config/nvim/local_plugins.vim"

" Download vim-plug if it's not already present.
" Remove these lines to disable automatic downloading of vim-plug
if !filereadable(expand(s:vim_plug_file))
    echom system("curl -fLo " . s:vim_plug_file . " --create-dirs "
    \ . "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim")
endif

" Specify a directory for plugins
call plug#begin(s:vim_plug_dir)

" To see the description/code for the plugin loaded by the line
"
"     Plug 'user/repo'
"
" go to https://github.com/user/repo.
Plug 'ctrlpvim/ctrlp.vim'
Plug 'gregsexton/gitv'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'tyok/nerdtree-ack'
Plug 'mklabs/split-term.vim'
Plug 'tpope/tpope-vim-abolish'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'moll/vim-bbye'
Plug 'ntpeters/vim-better-whitespace'
Plug 'rhysd/vim-clang-format'
Plug 'tpope/vim-fugitive'
Plug 'weynhamz/vim-plugin-minibufexpl'
Plug 'tpope/vim-sleuth'
Plug 'wesQ3/vim-windowswap'
Plug 'prettier/vim-prettier', {
  \ 'do': 'npm install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

Plug 'w0rp/ale'
Plug 'morhetz/gruvbox'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'vimoutliner/vimoutliner'
" Initialize plugin system
call plug#end()

"============================================
" Set leader. This is used for many mappings.
"============================================
let mapleader=","

"=========================================
" Settings/Mappings for particular plugins
"=========================================

"--------------
" Color/theme
"-------------
set termguicolors
colorscheme gruvbox
let g:airline_theme = "gruvbox"
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set background=dark

"----------
" switch buffers
"----------
nmap <C-n> :bn<CR>  " Next buffer in list
nmap <C-p> :bp<CR>  " Previous buffer in list
nmap <C-#> :b#<CR>  " Previous buffer you were in
nmap <C-3> :b#<CR>  " Previous buffer you were in

"----------
" ctrlp.vim
"----------
let g:ctrlp_working_path_mode = 'rw'
let g:ctrlp_custom_ignore = '\v[\/](build|doc|bazel-kcov|node_modules|dist)$'
" Launch CtrlP. Use this and then start typing fragments of the file path.
nnoremap <leader>ff :CtrlP<cr>

"------------
" vim-airline
"------------
set laststatus=2
set showtabline=2

"-----
" gitv
"-----
" Launch Gitv
nnoremap <leader>gv :Gitv<cr>

"---------
" nerdtree
"---------
" Launch NerdTree (file system viewer).
nnoremap <leader>nt :NERDTreeToggle<cr>
" Launch NerdTree with the current file selected.
nnoremap <leader>nf :NERDTreeFind<cr>

"-------------
" vim-fugitive
"-------------
" Show the diff to HEAD for the current file.
nnoremap <leader>gd :Gdiff<cr>
" Show the git status of the repo.
nnoremap <leader>gs :Gstatus<cr>

" Fold method
set foldmethod=syntax

"------------------------
" vim-windowswap Settings
"------------------------
let g:windowswap_map_keys = 0 "prevent default bindings
" Swap the contents of two windows. Press <leader>ss while in the first
" window, then navigate to the second window and press <leader>ss again.
nnoremap <silent> <leader>ss :call WindowSwap#EasyWindowSwap()<CR>

"===================================================================
" Other settings/mappings that are useful when working with Drake
"===================================================================

"-------------------------------------------
" Ignore CamelCase words when spell checking
"-------------------------------------------
fun! IgnoreCamelCaseSpell()
  syn match CamelCase /\<[A-Z][a-z]\+[A-Z].\{-}\>/ contains=@NoSpell transparent
  syn cluster Spell add=CamelCase
endfun
autocmd BufRead,BufNewFile * :call IgnoreCamelCaseSpell()

"---------------
" Misc. Settings
"---------------
" Use filetype plugins.
filetype plugin on
" Do not highlight all search matches.
set nohlsearch
" Show line numbers
set number

"=============================================================
" Other settings/mappings that may be useful in general. YYMV.
"=============================================================

"---------------------
" Buffer configuration
"---------------------
set hidden
" Open next buffer in the current window.
nmap ff :bnext<CR>
" Open previous buffer in the current window.
nmap FF :bprevious<CR>

"-----------------------
" 'Ergonomics' mappings.
"-----------------------
" Make getting from INSERT to NORMAL less of a stretch.
imap jj <Esc>
" Use <leader> + navigation keys to jump between windows in both NORMAL and
" INSERT modes.
nnoremap <leader>J <c-w>j
nnoremap <leader>K <c-w>k
nnoremap <leader>H <c-w>h
nnoremap <leader>L <c-w>l
inoremap <leader>J <Esc><c-w>j
inoremap <leader>K <Esc><c-w>k
inoremap <leader>H <Esc><c-w>h
inoremap <leader>L <Esc><c-w>l
" Save the current file.
nnoremap <leader>ww :w<cr>
" Close the current file.
nnoremap <leader>qq :q<cr>
" Save and close the current file.
nnoremap <leader>wq :w<cr>:q<cr>
" Close all files.
nnoremap <leader>qa :qa<cr>
" Use <leader><leader> as a replacement for ":".
nnoremap <leader><leader> :



" -----------------
"  Prettier
"  ----------------
"
let g:prettier#config#print_width = 120
let g:prettier#config#tab_width = 4
let g:prettier#config#use_tabs = 'false'
let g:prettier#config#semi = 'true'
let g:prettier#config#single_quote = 'true'
let g:prettier#config#bracket_spacing = 'true'
let g:prettier#config#jsx_bracket_same_line = 'false'
let g:prettier#config#arrow_parens = 'avoid'
let g:prettier#config#trailing_comma = 'all'
let g:prettier#config#parser = 'babylon'
let g:prettier#config#config_precedence = 'file-override'
let g:prettier#config#prose_wrap = 'preserve'
let g:prettier#config#arrow_parens = 'always'

" -----------------
"  Ale eslint
"  ----------------
"

let b:ale_fixers = {'javascript': ['prettier', 'eslint']}
let g:ale_javascript_prettier_options = '--print-width 100'                     "Set max width to 100 chars for prettier
nnoremap <leader>p :ALEFix<cr>


"----------------------------------
" Terminal management (Neovim only)
"----------------------------------
" Open a new terminal in a horizontal split.
nnoremap <leader>tj :Term<cr><C-\><C-n><c-w>x<c-w>ji
" Open a new terminal in a vertical split.
nnoremap <leader>tl :VTerm<cr>
" Go from TERMINAL mode to NORMAL mode.
tnoremap <leader>tq <C-\><C-n>
" Go to the next tab while in TERMINAL mode.
tnoremap <leader>gt <C-\><C-n>gt
" Move to an adjacent window while in TERMINAL mode.
tnoremap <leader>J <C-\><C-n><c-w>j<Esc>
tnoremap <leader>K <C-\><C-n><c-w>k<Esc>
tnoremap <leader>H <C-\><C-n><c-w>h<Esc>
tnoremap <leader>L <C-\><C-n><c-w>l<Esc>
let g:disable_key_mappings=1
set splitright
autocmd TermOpen * setlocal nonumber

"----------------------
" Edit/source this file
"----------------------
let s:current_file=expand('<sfile>:p')
if !exists("*EditVimrc")
  function EditVimrc()
    execute 'split' s:current_file
  endfunction
endif
if !exists("*SourceVimrc")
  function SourceVimrc()
    execute 'source' s:current_file
  endfunction
endif
" Edit this file.
nnoremap <leader>ev :call EditVimrc()<CR>
" Source this file.
nnoremap <leader>sv :call SourceVimrc()<CR>

